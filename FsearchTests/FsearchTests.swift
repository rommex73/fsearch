//
//  FsearchTests.swift
//  FsearchTests
//
//  Created by Roman on 11/8/19.
//  Copyright © 2019 Roman. All rights reserved.
//

import XCTest
@testable import Fsearch

class FsearchTests: XCTestCase {
    
    var bundle: Bundle!
    
    override func setUp() {
        bundle = Bundle(for: type(of: self))
    }
    
    func test_AsyncTask_Success() {
        let expect = self.expectation(description: "Expecting successful UIImage production")
        
        let _ = AsyncTask<UIImage>(consumer: { image in
            guard image != nil else { XCTFail("image is nil"); fatalError() }
            guard image!.size == CGSize (width: 360, height: 360) else { XCTFail("image size is wrong"); fatalError() }
            expect.fulfill()
        },
                                      producer: { _ in
                                        Thread.sleep(forTimeInterval: 1.0)
                                        let image = UIImage(named: "test_image", in: self.bundle, compatibleWith: nil)!
                                        return image
                                        
        }
        )
        
        wait(for: [expect], timeout: 2.0)
    }
    
    func test_AsyncTask_Cancelled() {
        let expect = self.expectation(description: "Expecting no call to Consumer function because the work was cancelled")
        
        let task = AsyncTask<UIImage>(consumer: { image in
            XCTFail("Should not call this closure because was cancelled")
        },
                                      producer: { _ in
                                        Thread.sleep(forTimeInterval: 2.0)
                                        let image = UIImage(named: "test_image", in: self.bundle, compatibleWith: nil)!
                                        return image
        }
        )
        Thread.sleep(forTimeInterval: 1)
        task.cancel()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
            expect.fulfill()
        }
        wait(for: [expect], timeout: 3.0)
        
    }
    
    func test_AsyncTask_Failed() {
        let expect = self.expectation(description: "Expecting nil because the work failed to deliver an image")
        
        let _ = AsyncTask<UIImage>(consumer: { image in
            guard image == nil else { XCTFail("Should be nil because was cancelled"); fatalError() }
            expect.fulfill()
            
        },
                                      producer: { _ in
                                        Thread.sleep(forTimeInterval: 2.0)
                                        let image = UIImage(named: "test_image_NON_EXISTENT_NAME", in: self.bundle, compatibleWith: nil)
                                        return image
        }
        )
        wait(for: [expect], timeout: 3.0)
    }
    
    func test_JSONParser() {
        let networkHelper = NetworkingHelper_Mockup(searchText: "kitten", bundle: bundle)
        let jsonProcessor = JSONProcessor (networkHelper: networkHelper)
        
        jsonProcessor.getURLArray { arrayResult in
            switch arrayResult {
            case .failure: XCTFail("error parsing")
            case .success(let array):   XCTAssert(array.count == 100)
            XCTAssert(array[0].absoluteString == "http://farm66.static.flickr.com/65535/49032680762_56edb36505.jpg")
            }
        }
    }
    
    func test_Cache() {
        let image = UIImage(named: "test_image", in: bundle, compatibleWith: nil)!
        let url = URL(string: "http://domain.com/test_image.png")!
        
        
        
        PhotoViewCell.ImageCache.shared.put(image, for: url)
        
        let fetched = PhotoViewCell.ImageCache.shared.fetch(for: url)
        XCTAssertNotNil(fetched)
    }
}

struct NetworkingHelper_Mockup: NetworkingHelperProtocol {
    let ASYNCRONOUS_DELAY = 2.0
    
    var searchText: String
    var bundle: Bundle
    func evokeRequest(_ completion: @escaping (Data?) -> ()) {
        Thread.sleep(forTimeInterval: ASYNCRONOUS_DELAY)
        let dataURL = bundle.url(forResource: "server_response", withExtension: "json")!
        let data = try! Data(contentsOf: dataURL)
        completion(data)
    }
}
