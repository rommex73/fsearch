//
//  History.swift
//  Fsearch
//
//  Created by Roman on 11/12/19.
//  Copyright © 2019 Roman. All rights reserved.
//

import Foundation
import UIKit

struct History {
    struct Item {
        var word: String
    }
    
    private var items: [Item] = []
    var itemCount: Int {
        items.count
    }
    
    func getWord(at index: Int) -> String {
        items[index].word
    }
    
    mutating func submit (new word: String) {
        if let alreadyExistsIndex = items.firstIndex(where: { $0.word == word } ) {
            items.remove(at: alreadyExistsIndex)
        }
        let item = Item(word: word)
        items.insert(item, at: 0)
    }
}

extension SearchResultsViewController {
    internal func setHistoryTableHidden(_ shouldHide: Bool) {
        guard isHistoryTableHidden != shouldHide else { return }
        
        DispatchQueue.main.async {
            // showing max 3 rows for history
            let height: CGFloat = shouldHide ? 1 : CGFloat( min( self.history.itemCount, 3 )) * self.HISTORY_CELL_HEIGHT + 1
            self.historyTableHeight.constant = height
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
            if !shouldHide { // scrolling up to the top if revealing the history
                if self.history.itemCount > 0 {
                    self.historyTableView.scrollToRow(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
                }
            }
            self.isHistoryTableHidden = shouldHide
        }
    }
}

extension SearchResultsViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let word = history.getWord(at: indexPath.row)
        self.searchBar.text = word
        self.onNewSearchTextEntered(word, delay: 0)
    }
}

extension SearchResultsViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return history.itemCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell") as! HistoryTableViewCell
        cell.titleLabel.text = history.getWord(at: indexPath.row)
        return cell
    }
}


class HistoryTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
}
