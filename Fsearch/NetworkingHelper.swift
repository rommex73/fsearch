//
//  NetworkingHelper.swift
//  PhotoSearch
//
//  Created by Roman on 11/8/19.
//  Copyright © 2019 Roman. All rights reserved.
//

import Foundation

enum NetworkingError: Error {
    case NoRepsonse
}

protocol NetworkingHelperProtocol {
    func evokeRequest (_ completion: @escaping (Data?) -> () )
}

struct NetworkingHelper: NetworkingHelperProtocol {
    var url: URL
    
    func evokeRequest (_ completion: @escaping (Data?) -> () ) {
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 5)
        request.httpMethod = "POST"
        let httpRequestTask = URLSession.shared.dataTask (with: request) { data, response, error in
            guard let serverResponse = data, error == nil else {
                completion (nil)
                return
            }
            completion (serverResponse)
        }
        httpRequestTask.resume()
    }
}


