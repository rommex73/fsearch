//
//  JSONProcessor.swift
//  PhotoSearch
//
//  Created by Roman on 11/8/19.
//  Copyright © 2019 Roman. All rights reserved.
//

import Foundation


struct JSONProcessor {
    var networkHelper: NetworkingHelperProtocol
    
    func getURLArray(_ completion: @escaping URLArrayConsumer)  {
         networkHelper.evokeRequest { data in
            guard let data = data else {
                completion( Result<[URL], MyFailure>.failure(.NertworkingError) )
                return
            }
            if let dict1 = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                if let dict2 = dict1["photos"]! as? [String: Any] {
                    if let array = dict2["photo"] as? [[String: Any]] {
                        let urlArray: [URL] =  array.compactMap {
                            let id = $0["id"] as? String
                            let server = $0["server"] as? String
                            let farm = $0["farm"] as? Int
                            let secret = $0["secret"] as? String
                            return ImageURLComposer(id: id, server: server, farm: farm, secret: secret)?.url
                        }
                        completion( Result<[URL], MyFailure>.success(urlArray))
                        return
                    }
                }
            }
            completion( Result<[URL], MyFailure>.failure(.JSONParsingError) )
        }
    }
}

struct ImageURLComposer {
    var url: URL?
    init? (id: String?, server: String?, farm: Int?, secret: String?) {
        guard let id = id, let server = server, let farm = farm, let secret = secret else { return nil }
        url = URL(string: "http://farm\(farm).static.flickr.com/\(server)/\(id)_\(secret).jpg")
    }
}
