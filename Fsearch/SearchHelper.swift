//
//  SearchHelper.swift
//  PhotoSearch
//
//  Created by Roman on 11/8/19.
//  Copyright © 2019 Roman. All rights reserved.
//


import Foundation

typealias URLArrayConsumer = (Result<[URL], MyFailure >) -> Void

class SearchHelper {
    var callback: URLArrayConsumer
    var searchWorkItem: DispatchWorkItem?
    var delay: Double
    
    init (searchText: String, delay: Double,  _ callback: @escaping URLArrayConsumer ) {
        self.callback = callback
        self.delay = delay
        
        searchWorkItem = DispatchWorkItem(qos: .background) { [weak self]  in
            
            // start a new searchWorkItem
            guard let url = self?.compoundURL(for: searchText) else {
                return
            }
            
            let networkHelper: NetworkingHelperProtocol = NetworkingHelper(url: url)
            let jsonProcessor = JSONProcessor (networkHelper: networkHelper)
            
            jsonProcessor.getURLArray { array in
                self?.callback(array)
                self?.searchWorkItem = nil
            }
        }
        DispatchQueue.global().asyncAfter(deadline: .now() + delay, execute: searchWorkItem!)
    }
    
    deinit {
        searchWorkItem?.cancel()
    }
    
    func compoundURL(for searchTerm: String) -> URL? {
      guard let escapedTerm = searchTerm.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) else {
        return nil
      }
      
      let urlString = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(SearchResultsViewController.FLICKR_API_KEY)&text=\(escapedTerm)&format=json&nojsoncallback=1" //&per_page=20
      return URL(string: urlString)
    }
}
