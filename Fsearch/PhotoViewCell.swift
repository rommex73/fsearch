//
//  PhotoViewCell.swift
//  PhotoSearch
//
//  Created by Roman on 11/8/19.
//  Copyright © 2019 Roman. All rights reserved.
//

import Foundation
import UIKit

typealias UIImageConsumer = ((UIImage) -> Void)

class PhotoViewCell: UICollectionViewCell {
    
    class ImageCache: Cache<URL> {
        static let shared = ImageCache()
        private init() {
            let cachesDirectoryUrl = FileManager.default.urls (for: FileManager.SearchPathDirectory.cachesDirectory, in: FileManager.SearchPathDomainMask.allDomainsMask).first!
            super.init(cacheDirectoryUrl: cachesDirectoryUrl, adapter: { $0.keyString } )
        }
    }
    
    static let queue = DispatchQueue(label: "image_working_queue", qos: .background, attributes: .concurrent)
    
    var imageFetcher: AsyncTask<UIImage>?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    func configure(for url: URL) {
        // start clean
        self.imageView.image = nil
        
        PhotoViewCell.queue.async {
            self.imageFetcher?.cancel()
            
            if let image = ImageCache.shared.fetch (for: url) { // get image either from the cache
                self.submit(image: image)
            } else { // or from the web
                self.activityStart()
                self.imageFetcher =
                    AsyncTask(consumer: { image in self.submit(image: image) },
                              producer: { isCancelled in
                                defer   {  self.activityStop()  }
                                if isCancelled() { return nil } // exit early if cancelled
                                guard   let data = try? Data(contentsOf: url),
                                    let image = UIImage(data: data) else { return nil }
                                ImageCache.shared.put(image, for: url)
                                if isCancelled() { return nil } // exit early if cancelled
                                return image
                    } )
            }
        }
    }
    
    private func submit(image: UIImage?) {
        DispatchQueue.main.async{
            self.imageView.image = image
        }
    }
}

extension PhotoViewCell {
    private func activityStart() {
        DispatchQueue.main.async{
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
        }
    }
    
    private func activityStop() {
        DispatchQueue.main.async{
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
        }
    }
}

fileprivate extension URL {
    var keyString: String{
        self.lastPathComponent
    }
}
