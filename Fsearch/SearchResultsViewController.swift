//
//  ViewController.swift
//  PhotoSearch
//
//  Created by Roman on 11/8/19.
//  Copyright © 2019 Roman. All rights reserved.
//

import UIKit

enum MyFailure: Error {
    case JSONParsingError, NertworkingError
}

class SearchResultsViewController: UIViewController {

    static let FLICKR_API_KEY = "cadf87f74d14081f132f44cea05e53b9"
    static let SEARCH_DELAY_SEC: Double = 1.5

    @IBOutlet weak var searchBar: UISearchBar!
    
    var history = History()
    @IBOutlet weak var historyTableView: UITableView!
    let HISTORY_CELL_HEIGHT: CGFloat = 44
    @IBOutlet weak var historyTableHeight: NSLayoutConstraint!
    var isHistoryTableHidden = true
    
    @IBOutlet weak var resultCollectionView: UICollectionView!
    private let cellMargin: CGFloat = 4
    private let itemsPerRow: CGFloat = 2
   
    var searchHelper: SearchHelper?
    var urlArray: [URL] = [] {
        didSet {
            DispatchQueue.main.async {
                // new URLS have arrived
                self.resultCollectionView.reloadData()
                self.resultCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
                
                // empty data should not hide the history options
                if !self.urlArray.isEmpty { self.setHistoryTableHidden(true) }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        
        resultCollectionView.dataSource = self
        resultCollectionView.delegate = self
        resultCollectionView.decelerationRate = .normal
        
        historyTableView.delegate = self
        historyTableView.dataSource = self
    }
    
    @IBAction func didTapCollectionView(_ sender: UITapGestureRecognizer) {
        searchBar.resignFirstResponder()
    }
        
    func emptyDisplay() {
        searchHelper = nil
        urlArray = []
    }
    
    func onNewSearchTextEntered(_ searchText: String, delay: Double) {
        guard !searchText.isEmpty else {
            emptyDisplay()
            setHistoryTableHidden(false)
            return }
        
        // detected a new word
        searchHelper = SearchHelper(searchText: searchText, delay: delay) { [weak self] arrayResult in
            switch arrayResult {
            case .success(let array):   self?.urlArray = array
                                        self?.indicateError(nil)
                                        self?.history.submit(new: searchText)
                                        DispatchQueue.main.async {
                                            self?.historyTableView.reloadData()
                                            self?.searchBar.resignFirstResponder()
                                        }
                
            case .failure(let error):   self?.emptyDisplay()
                                        self?.indicateError(error)
            }
        }
    }
    
    private func indicateError(_ error: MyFailure?) {
        DispatchQueue.main.async {
            if error != nil {
                // error indication is a stub, could be more specific
                self.resultCollectionView.backgroundColor = UIColor.red
            } else {
                self.resultCollectionView.backgroundColor = UIColor.systemBackground
            }
        }
    }
    
    
}


// MARK: UISearchBarDelegate
extension SearchResultsViewController: UISearchBarDelegate {    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        onNewSearchTextEntered(searchBar.text ?? "", delay: 0)
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.setHistoryTableHidden(false)
        onNewSearchTextEntered(searchText, delay: SearchResultsViewController.SEARCH_DELAY_SEC)
    }
}


// MARK: UICollectionViewDataSource
extension SearchResultsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return urlArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: PhotoViewCell.self),
                                                      for: indexPath) as! PhotoViewCell
        cell.configure (for: urlArray[indexPath.row])
        return cell
    }
}


// MARK: - Collection View Flow Layout Delegate
extension SearchResultsViewController : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = cellMargin * (itemsPerRow + 1)
        let availableWidth = resultCollectionView.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        let itemHeight = widthPerItem
        
        return CGSize(width: widthPerItem, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return cellMargin
    }
}



