//
//  Cache.swift
//  PhotoSearch
//
//  Created by Roman on 11/8/19.
//  Copyright © 2019 Roman. All rights reserved.
//

import Foundation
import UIKit

protocol CacheProtocol {
    associatedtype A
    var cacheDirectoryUrl: URL { get }
    var adapter: ((A) -> String) { get }
}

class Cache<A>: CacheProtocol {
    var cacheDirectoryUrl: URL
    var adapter: ((A) -> String)
    var JPEG_QUALITY: CGFloat = 0.9
    
    init(cacheDirectoryUrl: URL, adapter: @escaping (A) -> String ) {
        self.cacheDirectoryUrl = cacheDirectoryUrl
        self.adapter = adapter
    }
    
    func fetch (for uniqueKey: A) -> UIImage? {
        return fetch(for: adapter(uniqueKey))
    }
    
    func fetch (for key: String) -> UIImage? {
        let cachedImageURL = getCacheURL(for: key)
        if let data = try? Data(contentsOf: cachedImageURL, options: []) {
            return UIImage(data: data)
        }
        return nil
    }
    
    func put (_ image: UIImage, for uniqueKey: A) {
        do {
            try put(image, for: adapter(uniqueKey))
        }
        catch {
            print("ERROR: writing failed")
        }
    }
    
    func put (_ image: UIImage, for key: String) throws {
        let fileUrl = getCacheURL(for: key)
        guard let data = image.jpegData(compressionQuality: JPEG_QUALITY) else { return }
        try data.write(to: fileUrl)
    }
    
    private func getCacheURL(for key: String) -> URL {
        cacheDirectoryUrl.appendingPathComponent("cached_" + key)
    }
    
}
