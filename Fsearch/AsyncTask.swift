//
//  AsyncTask.swift
//  Fsearch
//
//  Created by Roman on 11/10/19.
//  Copyright © 2019 Roman. All rights reserved.
//

import Foundation

class AsyncTaskQueue {
    // OperationQueue that is used for long 'Producer' work
    static let q = OperationQueue()
    
    // serial queue that syncronizes access to isCancelled properties
    static let controllingQueue = DispatchQueue(label: "controller")
}

class  AsyncTask<A> {
    typealias CancellationIndicator = () -> Bool
    
    typealias Consumer = (A?) -> Void // function that takes the result
    typealias Producer = (CancellationIndicator) -> A? // function that creates the result being sensitive to cancellation
    
    private var isCancelled = false
    
    private var consumerFunction: ( Consumer )?
    private var producerFunction: Producer
    
    init (consumer: Consumer?, producer: @escaping Producer) {
        self.consumerFunction = consumer
        self.producerFunction = producer
        
        AsyncTaskQueue.q.addOperation {
            let result = self.producerFunction( { return self.isCancelled } )
            self.resolver (result)
        }
    }
    
    private func resolver (_ result: A?) {
        // evokes Consumer function only if not cancelled
        var shouldContinue = true
        AsyncTaskQueue.controllingQueue.sync {
            shouldContinue = !isCancelled
        }
        if shouldContinue {
            consumerFunction?(result)
        }
    }
    
    func cancel() {
        AsyncTaskQueue.controllingQueue.async {
            self.isCancelled = true
        }
    }
}
